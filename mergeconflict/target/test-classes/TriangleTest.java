package geometry;
import static org.junit.Assert.AssertEquals;
import org.junit.Test;

public class TriangleTest {
    Triangle t = new Triangle (2, 3);

    @Test
    public void checkGetMethods(){
        AssertEquals(2, t.getBase());
        AssertEquals(3, t.getHeight());
    }

    @Test
    public void checkArea(){
        AssertEquals(3, t.getArea());
    }

    @Test
    public void checkToString(){
        System.out.println(t.toString());
    }
}
