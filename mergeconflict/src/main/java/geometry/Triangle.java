package geometry;

public class Triangle {
    private double base;
    private double height;

    public Triangle(double b, double h){
        this.base = b;
        this.height = h;
    }

    public double getBase(){
        return this.base;
    }

    public double getHeight(){
        return this.height;
    }

    public double getArea(){
        return ((this.base * this.height)/2);
    }

    public String toString(){
        String s = "";
        s += "Area of triangle is: "+ getArea();

        return s;
    }
    
}
