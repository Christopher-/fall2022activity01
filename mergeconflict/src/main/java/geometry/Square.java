package geometry;

public class Square {
    private double side;

    public Square(double side) {
        this.side = side;
    }
    public double getArea() {
        return this.side * this.side;
    }
    public double getSide() {
        return this.side;
    }
    public String toString() {
        return "Square with side " + this.side;
    }
}
